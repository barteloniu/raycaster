const path = require("path")

const dist = path.resolve(__dirname, "dist")

module.exports = {
    mode: "development",
    entry: "./src/index.ts",
    devtool: "inline-source-map",
    devServer: {
        contentBase: dist
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: "ts-loader",
                exclude: /node_modules/
            }
        ]
    },
    resolve: {
        extensions: [
            ".ts", ".js"
        ]
    },
    output: {
        filename: "bundle.js",
        path: dist
    }
}