import Vector from "./vector";
import { PI, tan, cos } from "./math";
import { walls, gameScreen } from "./index";

export default class Camera{
    position: Vector
    direction: Vector
    plane: Vector

    constructor(x: number, y: number, angle: number, fov: number){
        this.position = new Vector(x, y)
        this.direction = Vector.fromAngle(angle)
        this.plane = Vector.fromAngle(angle + PI / 2).multiply(tan(fov/2))
    }

    check(): number[]{
        const result: number[] = []
        for(let i = 0; i < gameScreen.width; i++){
            result.push(this.castRay(this.direction.add(this.plane.multiply(i / gameScreen.width - 0.5))))
        }
        return result
    }

    castRay(rayDirection: Vector): number{
        let closest = Infinity
        walls.forEach(wall => {
            const x1 = wall.start.x,
            y1 = wall.start.y,
            x2 = wall.end.x,
            y2 = wall.end.y,
            x3 = this.position.x,
            y3 = this.position.y,
            x4 = this.position.x + rayDirection.x,
            y4 = this.position.y + rayDirection.y
            const t = ((x1 - x3) * (y3 - y4) - (y1 - y3) * (x3 - x4)) / ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4))
            const u = - ((x1 - x2) * (y1 - y3) - (y1 - y2) * (x1 - x3)) / ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4))
            if(0 < t && t < 1 && 0 < u){
                const collision = new Vector(x1 + t * (x2 - x1), y1 + t * (y2 - y1))
                const dist = this.position.distance(collision)
                closest = dist < closest ? dist : closest
            }
        })
        return closest * cos(rayDirection.toAngle() - this.direction.toAngle())
    }
}