export const { sin, cos, tan, atan2, sqrt, pow, PI } = Math
export const degToRad = (degrees: number): number => degrees * PI / 180