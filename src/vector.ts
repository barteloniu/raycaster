import { cos, sin, sqrt, pow, atan2 } from "./math";

export default class Vector{
    x: number
    y: number

    constructor(x: number, y: number){
        this.x = x
        this.y = y
    }

    rotate(angle: number): Vector{
        return new Vector(
            this.x * cos(angle) - this.y * sin(angle),
            this.x * sin(angle) + this.y * cos(angle)
        )
    }

    multiply(amount: number): Vector{
        return new Vector(this.x * amount, this.y * amount)
    }

    add(vector: Vector): Vector{
        return new Vector(this.x + vector.x, this.y + vector.y)
    }

    subtract(vector: Vector): Vector{
        return new Vector(this.x - vector.x, this.y - vector.y)
    }

    distance(point: Vector): number{
        return sqrt(pow(this.x - point.x, 2) + pow(this.y - point.y, 2))
    }

    toAngle(): number{
        return atan2(this.y, this.x)
    }

    static fromAngle(angle: number): Vector{
        return new Vector(cos(angle), sin(angle))
    }
}