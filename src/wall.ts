import Vector from "./vector";

export default class Wall{
    start: Vector
    end: Vector

    constructor(startX: number, startY: number, endX: number, endY: number){
        this.start = new Vector(startX, startY)
        this.end = new Vector(endX, endY)
    }
}