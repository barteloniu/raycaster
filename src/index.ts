import Camera from "./camera";
import { PI, degToRad } from "./math";
import Wall from "./wall";

export const gameScreen = {width: 640, height: 480}

export const walls = [new Wall(50, -20, 70, 20), new Wall(100, -50, 100, 50)]

const player = new Camera(0, 0, 0, degToRad(70))
console.log(player, player.check())

const ctx = (document.querySelector("#canvas") as HTMLCanvasElement).getContext("2d")

const keys: boolean[] = []

enum KeyNames{
    W = 87,
    A = 65,
    S = 83,
    D = 68
}

addEventListener("keydown", event => {
    console.log(event.keyCode)
    keys[event.keyCode] = true;
})

addEventListener("keyup", event => {
    delete keys[event.keyCode]
})

const rotateSpeed = 0.02

const update = () => {
    if(keys[KeyNames.W]) player.position = player.position.add(player.direction)
    if(keys[KeyNames.S]) player.position = player.position.subtract(player.direction)
    if(keys[KeyNames.A]) player.direction = player.direction.rotate(-rotateSpeed)
    if(keys[KeyNames.D]) player.direction = player.direction.rotate(rotateSpeed)
}

const render = () => {
    const distances = player.check()
    ctx.clearRect(0, 0, gameScreen.width, gameScreen.height)
    distances.forEach((distance, index) => {
        if(distance == Infinity) return
        const tmpC = 255 - (distance / gameScreen.width * 255)
        const tmpH = gameScreen.height - (distance / gameScreen.width * gameScreen.height)
        ctx.beginPath()
        ctx.moveTo(index, (gameScreen.height - tmpH) / 2)
        ctx.lineTo(index, (gameScreen.height + tmpH) / 2)
        ctx.strokeStyle = `rgb(0, ${tmpC}, ${tmpC})`
        ctx.stroke()
    });
}

const gameLoop = () => {
    update()
    render()
    requestAnimationFrame(gameLoop)
}
requestAnimationFrame(gameLoop)